class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :text
      t.references :post

      t.timestamps     
    end

    add_index :comments, :post_id
  end
end
